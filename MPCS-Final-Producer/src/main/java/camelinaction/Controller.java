package camelinaction;

import java.io.IOException;

public class Controller {
	
	public static void main(String[] args) throws InterruptedException, IOException {
		// TODO Auto-generated method stub
		Thread producer = new Thread(new FileCopierWithCamel());
		Thread thread1 = new Thread(new SplitByStock());
		Thread thread2 = new Thread(new CalStats());
		Thread thread3 = new Thread(new SplitByValue());
		Thread stateMachine = new Thread(new StateMachine());
		Thread tradingEngine = new Thread(new TradingEngine());
		Thread Z_stateMachine = new Thread(new Z_StateMachine());
		Thread Z_tradingEngine = new Thread(new Z_TradingEngine());
		Thread Y_stateMachine = new Thread(new Y_StateMachine());
		Thread Y_tradingEngine = new Thread(new Y_TradingEngine());
		
		
		producer.start();
		thread1.start();
		thread2.start();
		thread3.start();
		
		tradingEngine.start();
		Z_tradingEngine.start();
		Y_tradingEngine.start();
		stateMachine.start();
		Z_stateMachine.start();
		Y_stateMachine.start();
		
		producer.join();
		thread1.join();
		thread2.join();
		thread3.join();
		
		tradingEngine.join();
		Z_tradingEngine.join();
		Y_tradingEngine.join();
		stateMachine.join();
		Z_stateMachine.join();
		Y_stateMachine.join();
		
		ReportEngine re=ReportEngine.getInstance();
		re.printReport();

	}

}
