/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package camelinaction;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

import javax.jms.ConnectionFactory;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.component.jms.JmsComponent;

public class SplitByStock implements Runnable{

    public void run() {
        // create CamelContext
    	try{
        CamelContext context = new DefaultCamelContext();

        // connect to ActiveMQ JMS broker listening on localhost on port 61616
        ConnectionFactory connectionFactory = 
        	new ActiveMQConnectionFactory("tcp://localhost:62020");
        context.addComponent("jms",
            JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));
        
        // add our route to the CamelContext
        context.addRoutes(new RouteBuilder() {
            public void configure() {


            	//Split different stocks to different queues
                from("jms:queue:MPCS_51050_Final_Main")
                	.choice()
                		.when(body().regex("^.*MSFT.*$"))
                		.to("jms:queue:MPCS_51050_FINAL_TOPIC_MSFT")
                		.when(body().regex("^.*ORCL.*$"))
                		.to("jms:queue:MPCS_51050_FINAL_TOPIC_ORCL")
                		.when(body().regex("^.*IBM.*$"))
                		.to("jms:queue:MPCS_51050_FINAL_TOPIC_IBM")
                	.endChoice();
            }
        });

        // start the route and let it do its work
        context.start();
        Thread.sleep(20000);

        // stop the CamelContext
        context.stop();
    	}catch(Exception e){
    		
    	}
    }
}
