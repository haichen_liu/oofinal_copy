package camelinaction;

public class Iterator {
	public DataCollection[] dc=new DataCollection[100];
	public int count=0;
	public int current=0;
	public boolean isEmpty(){
		return count==0;
	}
	public double next(){	
		System.out.printf("current=%d, count=%d \n",current, count);
		for(int i=0;i<count;i++){
			System.out.printf("item=%f, ", dc[i].double_item);
		}
		
		if(current+1>=count){
			
			return 0;
		}else{
			current=current+1;
			System.out.printf("%f \n",dc[current].double_item);
			return dc[current].double_item;
		}
//		return current++>=count?null:dc[current];
	}
	public DataCollection next(int step){

		if(current+step>=count){
			return null;
		}else{
			current+=step;
			return dc[current];
		}
	}
	
	public boolean isDone(){
		return current+1>=count;
	}
	
	public void push_back(Double _d){
//		if(count+1==dc.length){
//			DataCollection[] new_dc=new DataCollection[count*2];
//			for(int i=0;i<=count;i++){
//				new_dc[i]=dc[i];
//			}
//			dc=new_dc;
//		}
//		
		dc[count]=new DataCollection(_d);
		count=count+1;
	}
	
	public double first(){
		current=0;
		return dc[current].double_item;
	}
}
