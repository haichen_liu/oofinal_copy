package camelinaction;

public class DataCollection {
	public int int_item;
	public double double_item;
	public String str_item;
	
	public DataCollection(int _int_item){
		int_item=_int_item;
	}
	public DataCollection(String _string_item){
		str_item=_string_item;
	}
	public DataCollection(double _double_item){
		double_item=_double_item;
	}
	
	public DataCollection(Double _double_item){
		double_item=_double_item.doubleValue();
	}

}
