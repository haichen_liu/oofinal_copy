/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package camelinaction;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

import javax.jms.ConnectionFactory;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.component.jms.JmsComponent;

public class SplitByValue implements Runnable{
	public static double MSFT_AVR;
	public static double ORCL_AVR;
	public static double IBM_AVR;
    public void run(){
        // create CamelContext
    	try{
        CamelContext context = new DefaultCamelContext();

        // connect to ActiveMQ JMS broker listening on localhost on port 61616
        ConnectionFactory connectionFactory = 
            	new ActiveMQConnectionFactory("tcp://localhost:62020");
            context.addComponent("jms",
                JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));
            
            // add our route to the CamelContext
            context.addRoutes(new RouteBuilder() {
                public void configure() {
                    //from("file:data/inbox?noop=true").to("file:data/outbox");
                //Split different values into different queues 
                	from("jms:queue:MPCS_51050_FINAL_TOPIC_MSFT")
                	.log("SUBSCRIBER RECEIVED: jms MSFT queue: ${body} from file: ${header.CamelFileNameOnly}")
                	.split(body(String.class).tokenize("\t"))
                	.choice()
                		.when(body().regex("^.*BidPrice.*$"))
                			.process(new Processor(){
		    					@Override
		    					public void process(Exchange exchange) throws Exception {
		    						String[] array = exchange.getIn().getBody(String.class).split(" ") ;
		    						exchange.getIn().setBody(array[2]);
		    						exchange.getIn().setHeader("BidPrice", "BidPrice");
		    					}
		                	})
                			.to("jms:topic:MSFT_BidPrice")
                			.when(body().regex("^.*BidQuantity.*$"))
                			.process(new Processor(){
		    					@Override
		    					public void process(Exchange exchange) throws Exception {
		    						String[] array = exchange.getIn().getBody(String.class).split(" ") ;
		    						exchange.getIn().setBody(array[2]);
		    						exchange.getIn().setHeader("BidQuantity", "BidQuantity");
		    					}
		                	})
                			.to("jms:topic:MSFT_BidQuantity")
                			.when(body().regex("^.*AskPrice.*$"))
                			.process(new Processor(){
		    					@Override
		    					public void process(Exchange exchange) throws Exception {
		    						String[] array = exchange.getIn().getBody(String.class).split(" ") ;
		    						exchange.getIn().setBody(array[2]);
		    						exchange.getIn().setHeader("AskPrice", "AskPrice");
		    					}
		                	})
                			.to("jms:topic:MSFT_AskPrice")
                			.when(body().regex("^.*AskQuantity.*$"))
                			.process(new Processor(){
		    					@Override
		    					public void process(Exchange exchange) throws Exception {
		    						String[] array = exchange.getIn().getBody(String.class).split(" ") ;
		    						String[] content = array[2].split("]");
		    						exchange.getIn().setBody(content[0]);
		    						exchange.getIn().setHeader("AskQuantity", "AskQuantity");
		    					}
		                	})
                			.to("jms:topic:MSFT_AskQuantity");
                	
                	
                	
                	from("jms:queue:MPCS_51050_FINAL_TOPIC_ORCL")
                	.log("SUBSCRIBER RECEIVED: jms MSFT queue: ${body} from file: ${header.CamelFileNameOnly}")
                	.split(body(String.class).tokenize("\t"))
                	.choice()
                		.when(body().regex("^.*BidPrice.*$"))
                		   	.process(new Processor(){
		    					@Override
		    					public void process(Exchange exchange) throws Exception {
		    						String[] array = exchange.getIn().getBody(String.class).split(" ") ;
		    						exchange.getIn().setBody(array[2]);
		    						exchange.getIn().setHeader("BidPrice", "BidPrice");
		    					}
		                	})
                			.to("jms:topic:ORCL_BidPrice")
                			.when(body().regex("^.*BidQuantity.*$"))
                				.process(new Processor(){
		    					@Override
		    					public void process(Exchange exchange) throws Exception {
		    						String[] array = exchange.getIn().getBody(String.class).split(" ") ;
		    						exchange.getIn().setBody(array[2]);
		    						exchange.getIn().setHeader("BidQuantity", "BidQuantity");
		    					}
		                	})
                			.to("jms:topic:ORCL_BidQuantity")
                			.when(body().regex("^.*AskPrice.*$"))
                			.process(new Processor(){
		    					@Override
		    					public void process(Exchange exchange) throws Exception {
		    						String[] array = exchange.getIn().getBody(String.class).split(" ") ;
		    						exchange.getIn().setBody(array[2]);
		    						exchange.getIn().setHeader("AskPrice", "AskPrice");
		    					}
		                	})
                			.to("jms:topic:ORCL_AskPrice")
                			.when(body().regex("^.*AskQuantity.*$"))
                			.process(new Processor(){
		    					@Override
		    					public void process(Exchange exchange) throws Exception {
		    						String[] array = exchange.getIn().getBody(String.class).split(" ") ;
		    						String[] content = array[2].split("]");
		    						exchange.getIn().setBody(content[0]);
		    						exchange.getIn().setHeader("AskQuantity", "AskQuantity");
		    					}
		                	})
                			.to("jms:topic:ORCL_AskQuantity");
                	
                	
                	from("jms:queue:MPCS_51050_FINAL_TOPIC_IBM")
                	.log("SUBSCRIBER RECEIVED: jms MSFT queue: ${body} from file: ${header.CamelFileNameOnly}")
                	.split(body(String.class).tokenize("\t"))
                	.choice()
                		.when(body().regex("^.*BidPrice.*$"))
                			.process(new Processor(){
		    					@Override
		    					public void process(Exchange exchange) throws Exception {
		    						String[] array = exchange.getIn().getBody(String.class).split(" ") ;
		    						exchange.getIn().setBody(array[2]);
		    						exchange.getIn().setHeader("BidPrice", "BidPrice");
		    					}
		                	})
                			.to("jms:topic:IBM_BidPrice")
                			.when(body().regex("^.*BidQuantity.*$"))
                			.process(new Processor(){
		    					@Override
		    					public void process(Exchange exchange) throws Exception {
		    						String[] array = exchange.getIn().getBody(String.class).split(" ") ;
		    						exchange.getIn().setBody(array[2]);
		    						exchange.getIn().setHeader("BidQuantity", "BidQuantity");
		    					}
		                	})
                			.to("jms:topic:IBM_BidQuantity")
                			.when(body().regex("^.*AskPrice.*$"))
                			.process(new Processor(){
		    					@Override
		    					public void process(Exchange exchange) throws Exception {
		    						String[] array = exchange.getIn().getBody(String.class).split(" ") ;
		    						exchange.getIn().setBody(array[2]);
		    						exchange.getIn().setHeader("AskPrice", "AskPrice");
		    					}
		                	})
                			.to("jms:topic:IBM_AskPrice")
                			.when(body().regex("^.*AskQuantity.*$"))
                			.process(new Processor(){
		    					@Override
		    					public void process(Exchange exchange) throws Exception {
		    						String[] array = exchange.getIn().getBody(String.class).split(" ") ;
		    						String[] content = array[2].split("]");
		    						exchange.getIn().setBody(content[0]);;
		    						exchange.getIn().setHeader("AskQuantity", "AskQuantity");
		    					}
		                	})
                			.to("jms:topic:IBM_AskQuantity");
                }
            });
          
        // start the route and let it do its work
        context.start();
        Thread.sleep(60000);

        // stop the CamelContext
        context.stop();
    	}catch(Exception e){
    		
    	}
    }
}
